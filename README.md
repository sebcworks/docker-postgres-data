PostgreSQL Data Container
=========================

Very simple Dockerfile to create a data container to hold PostgreSQL databases and (*possibly*) logs.

It creates 2 volumes:

* /data/postgresql
* /var/log

